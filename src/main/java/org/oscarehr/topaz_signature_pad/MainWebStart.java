package org.oscarehr.topaz_signature_pad;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;


public class MainWebStart
{
	public static void main(String... argv) throws Exception
	{
		// all libs named the same anyways
		System.loadLibrary("SigUsb");
		
		JFrame frame=new JFrame();
		frame.setTitle("Signature pad test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// all the following values are straight pass through so don't bother converting to native types.
		String sessionCookieKey=System.getProperty("sessionCookieKey");
		String sessionCookieValue=System.getProperty("sessionCookieValue");
		String signaturePadUrlBase=System.getProperty("jnlp.signaturePadUrlBase");
		String signatureRequestId=System.getProperty("jnlp.signatureRequestId");
		
		//---
		
		HttpPostSignatureHandler signatureCapturedHandler=new HttpPostSignatureHandler(signaturePadUrlBase, sessionCookieKey, sessionCookieValue, signatureRequestId);
		SignaturePanel panel=new SignaturePanel(500, 100, signatureCapturedHandler);
		frame.add(panel);
				
		//---
		
		frame.pack();

		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize=frame.getSize();
		frame.setLocation((screenSize.width-windowSize.width)/2, (screenSize.height-windowSize.height)/2);
		
		frame.setVisible(true);
	}
}