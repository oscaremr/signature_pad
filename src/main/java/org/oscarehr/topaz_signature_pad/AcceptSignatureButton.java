package org.oscarehr.topaz_signature_pad;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class AcceptSignatureButton extends JButton
{
	private SignaturePanel signaturePanel=null;
	
	public AcceptSignatureButton(SignaturePanel signaturePanel)
	{
		this.signaturePanel=signaturePanel;
		
		setText("Accept Signature");
		addActionListeneter();
	}

	private void addActionListeneter()
	{
		ActionListener listener=new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				signaturePanel.saveImage();
			}	
		};
		
		addActionListener(listener);
	}
}
