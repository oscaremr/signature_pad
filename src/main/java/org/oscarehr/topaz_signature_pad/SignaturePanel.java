package org.oscarehr.topaz_signature_pad;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import java.awt.event.*;


import com.topaz.sigplus.SigPlus;

public class SignaturePanel extends JPanel
{
	SigPlus sigPlus=null;
	private SignatureCapturedHandler signatureCapturedInterface=null;
	
	public SignaturePanel(final int width, final int height, SignatureCapturedHandler signatureCapturedInterface) 
	{
		final JComboBox connectionChooser;
		final JComboBox tabletChooser;

		this.signatureCapturedInterface=signatureCapturedInterface;
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints sigPlusConstraints = new GridBagConstraints();
		sigPlusConstraints.fill = GridBagConstraints.HORIZONTAL;
		sigPlusConstraints.gridx = 0;
		sigPlusConstraints.gridy = 0;
		sigPlusConstraints.gridwidth = 3;
		sigPlus=createSigPlus(width,height);
		add(sigPlus, sigPlusConstraints);
		
		GridBagConstraints acceptSignatureButtonConstraints = new GridBagConstraints();
		acceptSignatureButtonConstraints.gridx = 1;
		acceptSignatureButtonConstraints.gridy = 1;
		acceptSignatureButtonConstraints.weightx = 1;
		add(new AcceptSignatureButton(this), acceptSignatureButtonConstraints);

		GridBagConstraints clearSignatureButtonConstraints = new GridBagConstraints();
		clearSignatureButtonConstraints.gridx = 2;
		clearSignatureButtonConstraints.gridy = 1;
		clearSignatureButtonConstraints.weightx = 1;
		add(new ClearSignatureButton(this), clearSignatureButtonConstraints);	

		String[] tablets = 
        {
           "SignatureGem1X5",
           "SignatureGem4X5",
  		   "SignatureGemLCD",
   		   "SignatureGemLCD4X3",
        };
		tabletChooser= new JComboBox(tablets);
        tabletChooser.setSelectedIndex(0);
		GridBagConstraints tabletChooserConstraints = new GridBagConstraints();
		tabletChooserConstraints.gridx = 0;
		tabletChooserConstraints.gridy = 1;
		add(tabletChooser, tabletChooserConstraints);

		tabletChooser.addItemListener(new ItemListener(){
			  public void itemStateChanged(ItemEvent e){
				    
	                        if(tabletChooser.getSelectedItem() == "SignatureGem1x5"){
	                        	sigPlus.setTabletModel("SignatureGemLCD1x5");
		                        sigPlus.setImageXSize(width);
		                        sigPlus.setImageYSize(height);
	                        }
	                        else if (tabletChooser.getSelectedItem() == "SignatureGemLCD4X3"){
	                            sigPlus.setTabletModel("SignatureGemLCD4X3Old"); //properly set up LCD4X3
	                            sigPlus.setImageXSize(400);
	                            sigPlus.setImageYSize(200);
	                        }
	                        else if (tabletChooser.getSelectedItem() == "SignatureGemLCD"){
		                        sigPlus.setTabletModel("SignatureGemLCD"); //properly set up LCD4X3
			                    sigPlus.setImageXSize(width);
			                    sigPlus.setImageYSize(height);
		                    }
	                        else if (tabletChooser.getSelectedItem() == "SignatureGem4x5"){
		                        sigPlus.setTabletModel("SignatureGemLCD"); //properly set up LCD4X3
			                    sigPlus.setImageXSize(400);
			                    sigPlus.setImageYSize(400);
		                    }
	                     
			  }
		  });
		
	}
		
	private static SigPlus createSigPlus(int width, int height)
	{
		SigPlus sigPlus=new SigPlus();
		sigPlus.setTabletModel("SignatureGem1X5");
		sigPlus.setTabletComPort("HID1");
		sigPlus.setTabletState(1);
		sigPlus.setImageJustifyMode(4);
		sigPlus.setImagePenWidth(3);
		sigPlus.setPreferredSize(new Dimension(width, height));
		sigPlus.setImageXSize(width);
		sigPlus.setImageYSize(height);
		
		sigPlus.clearTablet();
		
		return(sigPlus);
	}
	
	protected void clearImage()
	{
		sigPlus.clearTablet();
	}
	
	protected void saveImage()
	{
		try
		{
			BufferedImage image=sigPlus.sigImage();
			
			ImageIO.setUseCache(false);
			
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			ImageIO.write(image, "jpg", os);
			signatureCapturedInterface.signatureCaptured(os.toByteArray());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}