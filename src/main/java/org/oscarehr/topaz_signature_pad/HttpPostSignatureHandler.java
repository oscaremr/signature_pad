package org.oscarehr.topaz_signature_pad;

import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpPostSignatureHandler implements SignatureCapturedHandler
{
	private String signaturePadUrlBase=null;
	private String sessionCookieKey=null;
	private String sessionCookieValue=null;
	private String signatureRequestId=null;
	
	public HttpPostSignatureHandler(String signaturePadUrlBase, String sessionCookieKey, String sessionCookieValue, String signatureRequestId)
	{
		this.signaturePadUrlBase = signaturePadUrlBase;
		this.sessionCookieKey = sessionCookieKey;
		this.sessionCookieValue = sessionCookieValue;
		this.signatureRequestId = signatureRequestId;
	}

	@Override
	public void signatureCaptured(byte[] image)
	{
        HttpClient httpclient = new DefaultHttpClient();
		try
		{	   
			HttpPost httpPut=new HttpPost(signaturePadUrlBase+"/uploadSignature.jsp?signatureRequestId="+signatureRequestId);
			httpPut.addHeader("Cookie", sessionCookieKey+'='+sessionCookieValue);
			
			ByteArrayEntity entity=new ByteArrayEntity(image);
			entity.setContentType("image/jpeg");
			httpPut.setEntity(entity);
			
	        ResponseHandler<String> responseHandler = new BasicResponseHandler();
	        httpclient.execute(httpPut, responseHandler);
	        
	        System.exit(0);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
	        httpclient.getConnectionManager().shutdown();     
		}
	}
}
