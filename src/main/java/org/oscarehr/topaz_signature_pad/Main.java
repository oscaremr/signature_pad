package org.oscarehr.topaz_signature_pad;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFrame;


public class Main
{
	public static void main(String... argv) throws Exception
	{
		JFrame frame=new JFrame();
		frame.setTitle("Signature pad test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//---
		
		SignatureCapturedHandler signatureCapturedHandler=new SignatureCapturedHandler()
		{
			@Override
			public void signatureCaptured(byte[] image)
			{
				String tmpDir=System.getProperty("java.io.tmpdir");
				FileOutputStream fos=null;
				try
				{
					fos=new FileOutputStream(tmpDir+"/temp_sig.jpg");
					fos.write(image);
					fos.flush();
					fos.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				finally
				{
					if (fos!=null)
						try
						{
							fos.close();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
				}
			}
		};
		
		SignaturePanel panel=new SignaturePanel(500, 100, signatureCapturedHandler);
		frame.add(panel);
				
		//---
		
		frame.pack();

		Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize=frame.getSize();
		frame.setLocation((screenSize.width-windowSize.width)/2, (screenSize.height-windowSize.height)/2);

		frame.setVisible(true);
	}
}