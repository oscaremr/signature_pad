package org.oscarehr.topaz_signature_pad;

public interface SignatureCapturedHandler
{
	/**
	 * This method will be called upon success of capturing a signature image.
	 * The bytes provided is a jpg version of the signature. You should store
	 * this image, i.e. to file or to database or do what ever you intend to do with
	 * the signature.
	 */
	public void signatureCaptured(byte[] image);
}
