package org.oscarehr.topaz_signature_pad;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ClearSignatureButton extends JButton
{
	private SignaturePanel signaturePanel=null;
	
	public ClearSignatureButton(SignaturePanel signaturePanel)
	{
		this.signaturePanel=signaturePanel;
		
		setText("Clear Signature");
		addActionListeneter();
	}

	private void addActionListeneter()
	{
		ActionListener listener=new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				signaturePanel.clearImage();				
			}	
		};
		
		addActionListener(listener);
	}	
}
