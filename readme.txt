--------
Overview
--------
This project is intended to be the client side application for enabling signature pad funtionality in oscar-caisi.

i.e. In oscar-caisi there is a desire to allow providers to use signature pads to electronically sign their notes. To do this we are currently thinking of using a java applet embedded in the web page. The web page will do the signature processing work, then upload/send the resulting jpg to the oscar/caisi system. This project is suppose to be the project to produce that applet portion of the code.

The first item to be worked on is the Topaz Systems Inc, 1x5 signature pad. In theory this project can be expanded 
to house signature pads from different vendors and types as well as other mechanisms like finger print readers.

-----
Build
-----
This is a fairly standard maven project. Build it using "mvn package".

The important results of the build are 
	- target/topaz_signature_pad-0.0-SNAPSHOT.jar
	- target/dependency/*.jar
	
----------------------
Running as a developer
----------------------
1) plug in the signature pad
2) chmod a+r /dev/hiddev0
3) run build_and_run.sh 

The above will run it as an application and save 
the image to /tmp.

if you want to see the java console run javaws -viewer, on the main window go to advanced, find it there.

------------------------
Installing for end users
------------------------
1) on linux only (windows is fine without this), /dev/hiddev0 will revert it's permissions upon reboot, you need to permanently set this. Have a search for /etc/udev/permissions.rules
2) java should be installed with plugin into the browser 
3) The application is network downloaded on the web page so no further work should be necessary

There maybe some setup that makes the usage of the signature pad sleeker. As an example, when you run it, accept the certificate permanently, and also set to "always run" for the web start. You can also get ride of the web start splash screen but that's more effort in configuring how the javaws plugin runs. 

-----------------
Source Code Notes
-----------------
The Main.java class is intended for development purposes only. The SignaturePanel.java
should provide all the functionality. The WebStartMain should be a thin wrapper
that just positions and initialises the SignaturePanel class
as a web start application. The WebStart is what is used on web pages / oscar-caisi.
