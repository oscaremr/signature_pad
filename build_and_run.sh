#!/bin/sh
mvn package

pushd target
	unzip -uj dependency/lib*.jar
	java -Djava.library.path=. -jar topaz_signature_pad-0.0-SNAPSHOT.jar 
popd
